#include "tzm.h"

MODULE_LICENSE("GPL");

static int ret_val = RETURN_VALUE;
module_param(ret_val, int, 0);

struct tzm_dev tzm_device;

dev_t devnm = 0;


static int tzm_open(struct inode *inode, struct file *filp)
{
    PDEBUG("In tzm_open angekommen");

    if (tzm_device.isopen == 1)
    {
        return -EBUSY;
    }
    tzm_device.isopen = 1;
    return nonseekable_open(inode, filp);
}

static int tzm_release(struct inode *inode, struct file *filp)
{
    PDEBUG("In tzm_release angekommen");
    tzm_device.isopen = 0;
    return 0;
}

static ssize_t tzm_read (struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
{
    char buf_tmp[30];
    int buf_len = 0;
    PDEBUG("In tzm_read angekommen");

    if (down_interruptible(&tzm_device.sem))
    {
        return -ERESTARTSYS;
    }

    if (tzm_device.last_measurement == -1)
    {
        PDEBUG("tzm_read: Noch keine Zeitmessung vorhanden");
        snprintf(buf_tmp,sizeof(buf_tmp),"%d\n",ret_val);
    }
    else
    {   
        PDEBUG("tzm_read: Zeitmessung bereits vorhanden");
        snprintf(buf_tmp,sizeof(buf_tmp),"%d\n",tzm_device.last_measurement);
    }

    buf_len = strlen(buf_tmp) - *f_pos;
    if (buf_len > count)
    {
        buf_len = count;
    }
    PDEBUG("tzm_read: buf_tmp: %s",buf_tmp);
    if (copy_to_user(buf,buf_tmp + *f_pos,buf_len))
    {
        up (&tzm_device.sem);
        return -EFAULT;
    }
    *f_pos += buf_len;


    PDEBUG("count: %u", (unsigned int)count);
    
    up (&tzm_device.sem);

    return buf_len;
}
static ssize_t tzm_write(struct file *filp, const char __user *buf, size_t count, loff_t *f_pos)
{
    unsigned long total_time;
    int ms_per_char;

    PDEBUG("In tzm_write angekommen");
    
    if (down_interruptible(&tzm_device.sem))
    {
        return -ERESTARTSYS;
    }

    PDEBUG("tzm_write: count: %u", (unsigned int) count);

    total_time = jiffies - tzm_device.last_endtime;
    tzm_device.last_endtime = jiffies;

    PDEBUG("tzm_write: total_time: %lu; HZ: %i", total_time, HZ);
    ms_per_char = ((total_time / HZ)*1000) / count;
    PDEBUG("tzm_write: ms_per_char: %i;", ms_per_char);
    tzm_device.last_measurement = ms_per_char;

    PDEBUG("tzm_write: Zeitberechnung erledigt");

    up(&tzm_device.sem);

    return count;
}

struct file_operations tzm_fops = {
    .owner =    THIS_MODULE,
    .llseek =   no_llseek,
    .read =     tzm_read,
    .write =    tzm_write,
    .open =     tzm_open,
    .release =  tzm_release,
};

void tzm_cleanup(void)
{
    PDEBUG("In tzm_cleanup angekommen");
    cdev_del(&tzm_device.cdev);

    /* cleanup_module is never called if registering failed */
    unregister_chrdev_region(devnm, 1);
}

static void tzm_setup_cdev(struct tzm_dev *dev)
{
    int err;
    PDEBUG("In tzm_setup_cdev angekommen");

    cdev_init(&dev->cdev, &tzm_fops);
    dev->cdev.owner = THIS_MODULE;
    dev->cdev.ops = &tzm_fops;
    err = cdev_add (&dev->cdev, devnm, 1);
    /* Fail gracefully if need be */
    if (err)
    {
        printk(KERN_NOTICE "Error %d adding tzm", err);
    }
}

int tzm_init(void)
{
    int result;
    PDEBUG("In tzm_init angekommen");

    tzm_device.isopen = 0;
    tzm_device.last_endtime = jiffies;
    tzm_device.last_measurement = -1;

    result = alloc_chrdev_region(&devnm, 0, 1, "tzm");

    if (result < 0)
    {
        printk(KERN_WARNING "tzm: can't get major\n");
        return result;
    }

    init_MUTEX(&tzm_device.sem);
    tzm_setup_cdev(&tzm_device);

    return 1;
}


module_init(tzm_init);
module_exit(tzm_cleanup);